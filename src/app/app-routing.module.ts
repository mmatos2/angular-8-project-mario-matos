import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { PaginaNoEncontradaComponent } from './pages/pagina-no-encontrada/pagina-no-encontrada.component';
import { HomeComponent } from './pages/home/home.component';
import { UsuariosComponent } from './pages/usuarios/usuarios.component';
import { VerUsuarioComponent } from './pages/ver-usuario/ver-usuario.component';
import { ImagesGuard } from './guards/images.guard';

const routes: Routes = [
  { path: 'home',
    loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule)
  },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent},
  { path: '', pathMatch: 'full', redirectTo: '/login' },
  { path: 'ver-usuario/:id/:name/:email/:username/:phone/:street/:suite/:zipcode/:website/:companyName/:catchPhrase', component: VerUsuarioComponent, canActivate: [ImagesGuard]},
  //{ path: 'usuarios', component: UsuariosComponent},
  { path: '**', component: PaginaNoEncontradaComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
