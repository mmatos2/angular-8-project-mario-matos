import { Component, OnInit } from '@angular/core';
import { Usuario } from './model/usuario';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public usuario : Usuario;
  public userValid : boolean;
  private username : string;
  private password : string;
  
  constructor(
    private router: Router
  ) {
    this.usuario = new Usuario("","");
    this.userValid = true;
    this.username = "test";
    this.password = "Ayi+2020"
   }

  ngOnInit(): void {
    
   
  }

  onSubmit()
  {
    //console.log(this.usuario);
    if(this.usuario.username === this.username && this.usuario.password === this.password){
      this.userValid = true;
      this.router.navigate(['/home']);
    }else{
      this.userValid = false;
    }
      

  }
}
