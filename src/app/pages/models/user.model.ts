export interface User {
    id?: string,
    name?: string;
    email?: string;
    phone?: string;
    username?: string;
    street?: string;
    suite?: string;
    zipcode?: string;
    website?: string;
    companyName?: string;
    catchPhrase?: string;
}