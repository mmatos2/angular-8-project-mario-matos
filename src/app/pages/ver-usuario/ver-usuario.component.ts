import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { User } from '../models/user.model';

@Component({
  selector: 'app-ver-usuario',
  templateUrl: './ver-usuario.component.html',
  styleUrls: ['./ver-usuario.component.css']
})
export class VerUsuarioComponent implements OnInit {

  public user : User;
  constructor(
    private _route : ActivatedRoute, //para recibir parametros de entrada
  ) { }

  ngOnInit(): void {
      this.user = {
        id: this._route.snapshot.params.id,
        name: this._route.snapshot.params.name,
        username: this._route.snapshot.params.username,
        email: this._route.snapshot.params.email,
        phone: this._route.snapshot.params.phone,
        street: this._route.snapshot.params.street,
        suite: this._route.snapshot.params.suite,
        zipcode: this._route.snapshot.params.zipcode,
        website: this._route.snapshot.params.website,
        companyName: this._route.snapshot.params.companyName,
        catchPhrase:this._route.snapshot.params.catchPhrase
      }
      //console.log(this.user);
  }

}
