import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-nuevo-usuario',
  templateUrl: './nuevo-usuario.component.html',
  styleUrls: ['./nuevo-usuario.component.css']
})
export class NuevoUsuarioComponent implements OnInit {
  @Output() agregar = new EventEmitter();
   
  public usuario = {
    name: '',
    email: '',
    phone: '',
    username: '',
    street: '',
    suite: '',
    zipcode: '',
    website: '',
    companyName: '',
    catchPhrase: ''
};

  constructor() { }

  ngOnInit(): void {
  }

  onSubmit(){
    
  }

  addUsuario($event){
    this.agregar.emit({user : this.usuario});
    alert('El usuario ' + this.usuario.username + ' fue creado con exito');
    this.limpiarCampos();
  }

  limpiarCampos(){
    this.usuario = null;
  }

}
