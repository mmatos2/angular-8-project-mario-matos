import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { PaginaNoEncontradaComponent } from './pagina-no-encontrada/pagina-no-encontrada.component';
import { SharedModule } from '../shared/shared.module';
import { PagesRoutingModule } from './pages-routing.module';
import { FormsModule } from '@angular/forms';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { VerUsuarioComponent } from './ver-usuario/ver-usuario.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { NuevoUsuarioComponent } from './nuevo-usuario/nuevo-usuario.component';


@NgModule({
  declarations: [HomeComponent, LoginComponent, PaginaNoEncontradaComponent, UsuariosComponent, VerUsuarioComponent, UsuarioComponent, NuevoUsuarioComponent],
  imports: [
    CommonModule,
    SharedModule,
    PagesRoutingModule,
    FormsModule
  ],
  exports: [HomeComponent, LoginComponent, PaginaNoEncontradaComponent,UsuariosComponent,VerUsuarioComponent]
})
export class PagesModule { }
