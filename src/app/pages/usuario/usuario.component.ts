import { Component, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../models/user.model';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent {

    @Input() user : User;
    @Input() indice : number;
    @Output() eliminar = new EventEmitter();
   
  constructor() { 
  }


  deleteUser(event)
  {
    console.log('indice: ' + this.indice);
    //this.index.emit({indice : this.user.id});
    this.eliminar.emit({indice : this.indice});
  }

}
