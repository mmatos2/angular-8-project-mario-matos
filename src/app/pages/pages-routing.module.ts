import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
// import { TodoListComponent } from '../features/components/todo-list/todo-list.component';
// import { TodoFormComponent } from '../features/components/todo-form/todo-form.component';


const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'usuarios', component: UsuariosComponent},
  { path: '', pathMatch: 'full', redirectTo: '/home' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }