import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';
import { Store } from '@ngrx/store';

import * as fromActionsUsers from '../store/users.actions';
import { UsersState } from '../store/users.reducers';


@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit  {

  public usersList : User[] = [];
  public isLoading = true;
  public userValid : boolean;
  public usuario = {
          name: '',
          email: '',
          phone: '',
          username: '',
          street: '',
          suite: '',
          zipcode: '',
          website: '',
          companyName: '',
          catchPhrase: ''
  };
  

  constructor(
    private store: Store<{ users: UsersState }>
  ) { 
    this.userValid = true;
  }
  

  ngOnInit(): void {
    this.store.dispatch(fromActionsUsers.FetchPending());
    this.store.subscribe( ({ users }) => {
      this.isLoading = users.pending;
      if(users.data && users.data.length) {
        //console.log(users.data);
        this.formatUsers(users.data);
        // console.log(this.usersList);
      }
    })
    if(this.usersList.length > 0)
    {
      this.isLoading = false;
    }
  }

  formatUsers(users : any){
    this.usersList = users.map(
      user =>{
        return{
          id: user.id,
          name: user.name,
          email: user.email,
          phone: user.phone,
          username: user.username,
          street: user.address.street,
          suite: user.address.suite,
          zipcode: user.address.zipcode,
          website: user.website,
          companyName: user.company.name,
          catchPhrase: user.company.catchPhrase
        }
      }
    );
  }

  deleteUser(i: number) : Array<User>
  {
    this.usersList.splice(i,1);
    return this.usersList;
  }

  eliminarUsuario(event):void{
    let r = confirm("¿Desea eliminar el usuario?");
    if (r == true) {
      this.deleteUser(event.indice);
    } 
  }

  nuevoUsuario($event):User[]{
    this.usersList.push($event.user);
    return this.usersList;
  }

}
