import { Injectable, OnDestroy } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class ImagesGuard implements CanActivate {

constructor(
  private _router : Router,
  ){ }


  //Debo validar que la ruta está recibiendo los datos del usuario como parámetro, 
  //caso contrario debe enviarlo nuevamente a la pantalla de Usuarios
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      let cantParametros = next.url.length;
      if(cantParametros == 12){
      // let id = next.url[1].path || null;
      // let name = next.url[2].path || null;
      // let email = next.url[3].path || null;
      // let username = next.url[4].path || null;
      // let phone = next.url[5].path || null;
      // let street = next.url[6].path || null;
      // let suite = next.url[7].path || null;
        
      return true;
      }else{
        this._router.navigate(['/usuarios']);
        return false;
      }

      
      // if(name === null || email === null || username === null || 
      //   phone === null || street === null || suite === null )
      // {
      //   this._router.navigate(['/usuarios']);
      //   return false;
      // }else{
      //   return true;
      // }
  }
  
}
