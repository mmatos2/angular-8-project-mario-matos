import { TestBed } from '@angular/core/testing';

import { ImagesGuard } from './images.guard';

describe('ImagesGuard', () => {
  let guard: ImagesGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ImagesGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
